import "./App.css";
import React,{useState} from 'react';
import Header from "./components/header/Header";
import {Redirect, Route, Switch } from 'react-router-dom';
import NewExpense from "./components/expenses/newexpense/NewExpense";
import AuthForm from "./components/Auth/AuthForm";
import LandingPage from "./components/landingpage/LandingPage";
import HomePage from "./components/homepage/HomePage";
import { useSelector } from "react-redux";
// import { authActions } from "./store";


const App = () => {
  const isLoggedNow = useSelector(state =>state.auth.isAuthenticated);
  const [showMenu,setShowMenu] = useState(false);

  const showMenuHandler = () => {
    setShowMenu(true)
    }

  const hideMenuHandler = () => {
    setShowMenu(false)
  }

  return <>
    <Header isMenuShown={showMenu} onShow={showMenuHandler} onClose={hideMenuHandler}/>
    <Switch>
      <Route path='/' exact >
          {isLoggedNow ? <HomePage /> : <LandingPage />}
      </Route>
      {!isLoggedNow && <Route path='/auth'>
          <AuthForm />
      </Route>}
      {isLoggedNow && <Route path='/newexpense'>
          <NewExpense />
    </Route>}
    <Route path="*">
      <Redirect to="/" />
    </Route>
    </Switch>
  </>
};

export default App;