import { useState, useRef } from 'react';
import classes from './AuthForm.module.css';
import { useDispatch } from 'react-redux';
import { authActions } from "../../store";
import { useHistory } from 'react-router-dom';

const AuthForm = () => {
  const [isLogin, setIsLogin] = useState(true);
  const emailInputRef = useRef();
  const passwordInputRef = useRef();
  // const isLoggedNow = useSelector(state => state.auth.isAuthenticated);
  const dispatch = useDispatch();
  const history = useHistory();

  const switchAuthModeHandler = () => {
    setIsLogin((prevState) => !prevState);
  };

  const submitHandler = async (e) => {
    e.preventDefault();
    let baseURL;
    const enteredEmail = emailInputRef.current.value;
    const enteredPassword = passwordInputRef.current.value;

    if(isLogin){
      baseURL = "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAc9Lsk6ctwhI0z4jpv1YnJFes9117CENk";
    }
    else{
      baseURL = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyAc9Lsk6ctwhI0z4jpv1YnJFes9117CENk';
    }
    
    const response = await fetch(baseURL,
      {
        method:'POST',
        body: JSON.stringify({
          email:  enteredEmail,
          password: enteredPassword,
          returnSecureToken: true
        }),
        headers:{
          'Content-Type' : 'application/json'
        }
      })
      const data = await response.json();
  
     if(data && data.error){
       alert(data.error.message)
     }else{
      dispatch(authActions.logIn(data.idToken));
      history.replace("/home");
     }

    }

  return (
    <section className={classes.auth}>
      <h1>{isLogin ? 'Login' : 'Sign Up'}</h1>
      <form onSubmit={submitHandler}>
        <div className={classes.control}>
          <label htmlFor='email'>Your Email</label>
          <input type='email' id='email' required ref={emailInputRef} />
        </div>
        <div className={classes.control}>
          <label htmlFor='password'>Your Password</label>
          <input type='password' id='password' required ref={passwordInputRef} />
        </div>
        <div className={classes.actions}>
          <button>{isLogin ? 'Login' : 'Create Account'}</button>
          <button
            type='button'
            className={classes.toggle}
            onClick={switchAuthModeHandler}
          >
            {isLogin ? 'Create new account' : 'Login with existing account'}
          </button>
        </div>
      </form>
    </section>
  );
};

export default AuthForm;
