import React from 'react'
import "./Menu.css";
import {Button} from 'react-bootstrap';
import { useSelector,useDispatch } from "react-redux";
import { authActions } from '../../../store';
import { Link,useHistory } from 'react-router-dom';

const Menu = (props) => {
    const direction = props.direction;
    const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
    const dispatch = useDispatch();
    const history = useHistory();

    const logOutHandler = () =>{
        dispatch(authActions.logOut());
        history.replace("/");
    }
    return (
            <div className='menu'>
                <nav className='menu-nav'>
                    {isAuthenticated && <ul style={{flexDirection:direction}}>
                        <li><Link to="/">Home</Link></li>
                        <li><Link to="/settings">Settings</Link></li>
                        <li>
                        <Link to="/newexpense">
                            Add New Expense
                        </Link>
                    </li>
                        <li><Button onClick={logOutHandler} variant="success">Logout</Button></li>
                    </ul>}
                    {!isAuthenticated && <ul style={{flexDirection:direction}}>
                        <li> <Link to="/auth" >
                            <Button variant="success"> LogIn / SignUp </Button>
                        </Link></li>
                    </ul>}
                </nav>
            </div>
    )
}

export default Menu;