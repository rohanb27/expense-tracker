import React,{ useRef } from 'react';
import { Button, Container } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { Link,useHistory } from 'react-router-dom';
import { operationActions } from '../../../store';
import "./NewExpense.css";

const NewExpense = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const nameInputRef = useRef();
    const amountInputRef = useRef();
    const dateInputRef = useRef();
    const modeInputRef = useRef();


    const submitHandler = async (e) =>{
        e.preventDefault();
        const enteredName = nameInputRef.current.value;
        const enteredAmount = amountInputRef.current.value;
        const enteredDate = dateInputRef.current.value.replace("T", " ");
        const enteredMode = modeInputRef.current.value;
        console.log(enteredName, enteredAmount, enteredDate, enteredMode);

        if(enteredName.trim() === "" || enteredAmount.trim() === ""  || enteredDate.trim() === "" ){
            alert("Please fill all the details");
        }else{
            await fetch('https://expense-tracker-87e5f-default-rtdb.asia-southeast1.firebasedatabase.app/expenses.json',{
                method:"POST",
                body:JSON.stringify({
                    name:enteredName,
                    amount:enteredAmount,
                    date:enteredDate,
                    mode:enteredMode
                }),
                headers:{
                    'Content-type':'application/json'
                }
            })

            dispatch(operationActions.add());
            history.replace("/");
        }
        
    }
    

  return (
    <Container className='new-expense p-3 text-center'>
        <h3>Add new Expense</h3>
        <form className='expense-form'>
        <div className='m-4'>
            <label htmlFor="name" className='mb-2'>Name</label>
            <br />
            <input ref={nameInputRef} type="text" name="name" placeholder='What did you buy?' />
        </div>
        <div className='m-4'>
            <label htmlFor="amount" className='mb-2'>Amount</label>
            <br />
            <input type="text" name="Amount " placeholder='How much did you spent?' ref={amountInputRef} />
        </div>
        <div className='m-4'>
            <label htmlFor="date" className='mb-2'>Date</label>
            <br />
            <input type="datetime-local" name="date" placeholder='When did you buy?' ref={dateInputRef} />
        </div>
        <div className='m-4'>
            <label htmlFor="paymentmode" className='mb-2'>Mode of Payment</label>
            <br />
            <select class="form-select" aria-label="Default select example" placeholder='How did you pay?' ref={modeInputRef}>
                <option value="Cash">Cash</option>
                <option value="Online Wallets">Online Wallets</option>
                <option value="Debit Card">Debit Card</option>
                <option value="Credit Card">Credit Card</option>
                <option value="Other">Other</option>
            </select>
        </div>
        </form>
        <Link to="/" >
            <Button type="submit" onClick={submitHandler} class="btn btn-primary btn-lg mt-4">Submit</Button>
        </Link>
    </Container>
  )
}

export default NewExpense;