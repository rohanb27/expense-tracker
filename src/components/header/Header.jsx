import React from 'react';
import Menu from '../UI/menu/Menu';
import "./Header.css";
import Modal from '../UI/modal/Modal';
import { Link } from 'react-router-dom';
import Logo from "../../images/logo.jpg";

const Header = (props) => {
    const MenuShown = props.isMenuShown;
    const showMenuHandler = () => {
        props.onShow();
    } 

    return (
        <div className='header px-3'>
            <div className="header-container">
                <div className="header-title">
                <Link to="/">
                    <div className="logo">
                        <img src={Logo} alt="" />
                    </div>
                        
                </Link>
                </div>
                <div className="header-checkbtn">
                    {!MenuShown && <i className="fas fa-bars fa-2x" onClick={showMenuHandler}></i>}
                    {MenuShown && <i className="fa-solid fa-xmark fa-2x" onClick={props.onClose}></i>}
                    {MenuShown && <Modal onClose={props.onClose}><Menu direction={"column"}/></Modal>}
                </div>
                <div className="header-menu">
                    {/* Pass the direction props as row for the horizontal menu */}
                    <Menu direction={"row"}/>
                </div>
                
            </div>
        </div>
    )
}

export default Header
