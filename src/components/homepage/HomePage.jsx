import React, { useEffect,useState } from 'react';
import { Table, Container, Toast} from 'react-bootstrap';
import { useSelector } from 'react-redux';
import "./HomePage.css";
import { Link } from 'react-router-dom';

const HomePage = () => {
  const operation = useSelector(state => state.operation.currentOperation);
  const [expenses, setExpenses] = useState([]);
  const [show, setShow] = useState(true);
  
      const fetchExpenses = async () => {
        const response = await fetch('https://expense-tracker-87e5f-default-rtdb.asia-southeast1.firebasedatabase.app/expenses.json');
        const data =  await response.json();   
        
        const loadedExpenses = [];
        for(const key in data){
          loadedExpenses.push({
            id:key,
            name:data[key].name,
            amount:data[key].amount,
            date:data[key].date,
            mode:data[key].mode
          })}
          
          setExpenses(loadedExpenses);
        };
        
        useEffect(() => {
          fetchExpenses();
        },[]);

  return (
    <>
    {operation && <Toast position="top-center" onClose={() => setShow(false)} show={show} delay={3000} autohide className="mx-auto">
      <Toast.Header>
        <strong className="me-auto">Expense {operation} Successfully!!</strong>
        <small className="text-muted">just now</small>
      </Toast.Header>
    </Toast>}
  <Container className='mt-3'>
  <Table responsive="md" className='mt-33'>
    <thead className='align-top'>
      <tr>
        <th>#</th>
        <th>You Spent On</th>
        <th>How much you Spent</th>
        <th>When you Spent</th>
        <th>How you paid</th>
        <th>Operations</th>
      </tr>
    </thead>
    <tbody className='text-left'>
      {expenses.map((item,i) => (<tr>
        <td>{i+1}</td>
        <td>{item.name}</td>
        <td>Rs. {item.amount}</td>
        <td>{item.date}</td>
        <td>{item.mode}</td>
        <td><div className='icons'>
           <Link>
           <i class="fa-solid fa-pen-to-square"></i>
            </Link>        
            <Link>
              <i class="fa-solid fa-trash-can"></i>
            </Link>
          </div> </td>
      </tr>))}
    </tbody>
  </Table>
    </Container>
    </>
  )
}

export default HomePage;