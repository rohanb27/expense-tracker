import React from 'react';
import "./LandingPage.css";
import { Container, Row, Col,Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import HomeImg from "../../images/home2.png"

const LandingPage = () => {
  return (
    <div id='Landing'>
        <div className="Landing mx-4 mt-3 justify-content-center">
            <Container fluid>
                <Row  className='text-center'>
                    <Col md={4} className="my-5">
                        <h2 className='my-5'>Loking for a Simple Yet Outstanding Tracker?</h2>
                        <h3 className='mb-5'>Well...You're at the Right Place</h3>

                        <Link to="/auth">
                            <Button variant="success">Get Started Here</Button>
                        </Link>
                    </Col>
                    <Col md={8}>
                        <img className='landing-img mx-auto' src={HomeImg} alt="" />
                    </Col>
                </Row>

            </Container>
        </div>
    </div>
  )
}

export default LandingPage;