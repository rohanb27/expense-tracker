import { createSlice, configureStore } from "@reduxjs/toolkit";

const token = localStorage.getItem('token');
const authenticated =!!token;

const initialAuthState = {
    idToken : token,
    isAuthenticated: authenticated,
};
const authSlice = createSlice({
    name:'auth',
    initialState: initialAuthState,
    reducers:{
        logIn(state, action) {
            state.isAuthenticated = true;
            state.idToken =  action.payload;
            localStorage.setItem('token', action.payload);
        },
        logOut(state) {
            state.isAuthenticated = false;  
            state.idToken = null;
            localStorage.removeItem('token');
        }
    }
})

const operationSlice = createSlice({
    name:'operation',
    initialState: {
        currentOperation : "",
    },
    reducers:{
        add(state){
            state.currentOperation = "Added"
        },
        update(state){
            state.currentOperation = "Updated"
        },
        delete(state){
            state.currentOperation = "Deleted"
        }
    }
})

const store = configureStore({
    reducer: {
        auth: authSlice.reducer,
        operation: operationSlice.reducer
    }

});

export const authActions = authSlice.actions;
export const operationActions = operationSlice.actions;
export default store;